extends KinematicBody2D

const HALF_HEIGHT = 64
const HALF_WIDTH = 64

export var up_down_speed = 350
export var speed = 100

var bullet_scene = preload("res://bullet.tscn")
var screen_size

var alive = true

func start(respawning = false):
    alive = true
    $CollisionPolygon2D.set_deferred("disabled", false)
    show()

    # Blank screen between levels, unless it's Level 0
    position.x = -1920 # bad to have random hard numbers like this
    if global.current_level == 0:
        position.x = 0
    
    if respawning or global.current_level == 0:
        position.y = global.level_scene.starting_y

func shoot():
    var bullet = bullet_scene.instance()
    bullet.position = position + Vector2(HALF_WIDTH, 0)
    get_parent().add_child(bullet)
    $Reload.start()

# Called when the node enters the scene tree for the first time.
func _ready():
    add_to_group("player")
    screen_size = get_viewport_rect().size

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
    if not alive:
        return

    var velocity = Vector2()
    velocity.x += speed * delta

    if Input.is_action_pressed("ui_down"):
        velocity.y += 1
    if Input.is_action_pressed("ui_up"):
        velocity.y -= 1
        
    var collision = move_and_collide(velocity * delta * up_down_speed)
    if collision:
        alive = false
        $CollisionPolygon2D.set_deferred("disabled", true)
        hide()
        $Respawn.start()

    # screen_size.y isn't doing what I expected
    # This is really bad
    position.y = clamp(position.y, HALF_HEIGHT, 1080 - HALF_HEIGHT)

func _process(delta):    
    if alive and Input.is_action_pressed("ui_accept") and $Reload.time_left <= 0:
        # Make a bullet
        shoot()

func _on_Respawn_timeout():
    # Revert everything to its previous state
    get_tree().call_group("dynamic_props", "reset")
    start(true)
