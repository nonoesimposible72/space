"""
Report any positive integer as a sum of powers of 2.
"""

import argparse

def main():
    parser = argparse.ArgumentParser(description = "Report any positive integer as a sum of powers of 2")
    parser.add_argument("number", type=int)
    args = parser.parse_args()
    n = args.number
    if n < 1:
        print("Number must be a positive integer")
    
    numbers = ""
    f = n
    while f >= 1:
        numbers = str(f % 2) + numbers
        f = int(f / 2)
    print(numbers)
    
    for i, x in enumerate(list(numbers)):
        if x != "0":
            expr = "2^" + str(len(numbers) - i - 1)
            print(expr, "=", eval(expr.replace("^", "**")))

if __name__ == "__main__":
    main()
