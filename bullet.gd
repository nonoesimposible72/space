extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var speed = 2000

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
    var collision = move_and_collide(Vector2(speed * delta, 0))
    if collision:
        queue_free()
        if "Target" in collision.collider.name and collision.collider.enabled:
            # This is kinda inefficient, isn't it?
            for child in collision.collider.get_children():
                if child is KinematicBody2D and child.has_method("action"):
                    child.action()
            collision.collider.get_node("Polygon2D").hide()
            collision.collider.get_node("CollisionShape2D").set_deferred("disabled", true)

func _on_Life_timeout():
    queue_free()
