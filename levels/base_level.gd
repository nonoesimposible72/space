extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# Currently the game is not resizable
const HEIGHT = 1080.0
export (int) var width

export var starting_y = HEIGHT / 2

# Called when the node enters the scene tree for the first time.
#func _ready():
#    pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
# func _process(delta):
#    pass

func on_body_exited(body):
    if body.is_in_group("player") and body.position.x > width:
        print(body.name, " exited")
        print("player position MY position ", body.position.x, " ", position.x)

        print("Finished ", name)
        queue_free()
        global.increment_level()
