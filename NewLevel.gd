extends Label


func show():
    text = "Level " + str(global.current_level)
    visible = true
    $Timer.start()


func _on_Timer_timeout():
    visible = false
