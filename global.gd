extends Node

const NUM_LEVELS = 2
var level_scenes = []

var current_level = -1
var level_scene
   
# Called when the node enters the scene tree for the first time.
func _ready():
    Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
    
    for i in range(NUM_LEVELS):
        level_scenes.append(load("res://levels/level_" + str(i) + ".tscn"))
        
    increment_level()

func increment_level():
    current_level += 1
    
    level_scene = level_scenes[current_level].instance()
    get_node("/root/game").add_child(level_scene)
    print("Loaded Level ", current_level)
    get_node("/root/game/CanvasLayer/NewLevel").show()

    get_node("/root/game/Player").start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass
