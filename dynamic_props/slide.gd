extends "res://dynamic_props/dynamic_prop.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (int) var speed = 400
export (String, "up", "down") var direction = "up"

# Called when the node enters the scene tree for the first time.
func _ready():
    if direction == "up":
        speed = -speed

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    if not moving:
        return

    position.y += speed * delta
