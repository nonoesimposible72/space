extends "res://dynamic_props/dynamic_prop.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (float) var increment = PI / 5
export (bool) var invert

func _ready():
    if invert:
        increment = -increment

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    if not moving:
        return

    rotation += increment * delta
        
