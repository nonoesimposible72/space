extends KinematicBody2D

# OG stands for original, of course
var og_position
var og_rotation

var moving = false
export (bool) var enabled = true

func reset():
    position = og_position
    rotation = og_rotation
    
    moving = false

    if has_node("Polygon2D"):
        get_node("Polygon2D").show()
    if has_node("CollisionShape2D"):
        get_node("CollisionShape2D").set_deferred("disabled", false)

# Called when the node enters the scene tree for the first time.
func _ready():
    og_position = position
    og_rotation = rotation

    add_to_group("dynamic_props")

func action():
    moving = true
    enabled = true
